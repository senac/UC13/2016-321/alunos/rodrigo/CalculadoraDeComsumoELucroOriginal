/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradecomsumoelucro;

/**
 *
 * @author Diamond
 */
public class Corrida {

    private double kmInicio;
    private double kmFim;
    private double valor;

    public Corrida() {
    }

    public Corrida(double kmInicio, double kmFim, double valor) {
        this.kmInicio = kmInicio;
        this.kmFim = kmFim;
        this.valor = valor;
    }

    public double getKmInicio() {
        return kmInicio;
    }

    public void setKmInicio(double kmInicio) {
        this.kmInicio = kmInicio;
    }

    public double getKmFim() {
        return kmFim;
    }

    public void setKmFim(double kmFim) {
        this.kmFim = kmFim;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
    public double getTotalPercorrido(){
        return this.kmFim - this.kmInicio ; 
    }

}

