/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradecomsumoelucro;

/**
 *
 * @author Diamond
 */
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.List;

public class TaxiComplexo {

    private double odomentroAtual;
    private double combustivel;

    private List<Corrida> corridas = new ArrayList<>();

    public TaxiComplexo() {
    }

    public TaxiComplexo(double odomentroAtual, double combustivel) {
        this.odomentroAtual = odomentroAtual;
        this.combustivel = combustivel;
    }

    public double getOdomentroAtual() {
        return odomentroAtual;
    }

    public void setOdomentroAtual(double odomentroAtual) {
        this.odomentroAtual = odomentroAtual;
    }

    public double getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(double combustivel) {
        this.combustivel = combustivel;
    }

    public void novaCorrida(double distancia, double valor) {
        Corrida corrida = new Corrida(this.odomentroAtual, this.odomentroAtual + distancia, valor);
        this.corridas.add(corrida);
    }

    public double getConsumo() {

        double total = 0;

        // Streams e lambda expressions a partir do JAVA 8
        //  total = corridas.stream().map((corrida) -> corrida.getTotalPercorrido()).reduce(total, (accumulator, _item) -> accumulator + _item);
        for (Corrida corrida : corridas) {
            total += corrida.getTotalPercorrido();
        }

        return total / this.combustivel;
    }

    public double getLucro() {

        double valorTotal = 0;
        double totalPercorrido = 0;
        for (Corrida corrida : corridas) {
            valorTotal += corrida.getValor();
            totalPercorrido += corrida.getTotalPercorrido();
        }

        return valorTotal - (totalPercorrido * 1.9);
    }

}

